package ordenacion;

public class BubbleSort {
	
	
	
	public int[] sort(int[] recibido) {
		boolean ordenado = false;
		while(!ordenado) {
			boolean aunNoEstaOrdenado = true;
			for (int i = 0; i < recibido.length; i++) {	
				if(i < (recibido.length - 1)) {	
					if(recibido[i] > recibido[(i + 1)]) {
						int cambiar1 = recibido[i];
						int cambiar2 =  recibido[(i+1)];
						recibido[i] = cambiar2;
						recibido[(i+1)] = cambiar1;
						aunNoEstaOrdenado =false;
					}
				}	
			}
			if (aunNoEstaOrdenado) {
				ordenado = true;
			}
		}
		return recibido;
	}

}
