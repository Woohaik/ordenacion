package ordenacion;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

public class BubbleSortTest {

	@Test
	void test() {
		int[] ListaEsperada = new int[] { 1,1,1,3};
		int[] aordenar = new int[] { 1,3,1,1 };
		int[] listaOrdenada = aordenar;
		BubbleSort burbuja = new BubbleSort();
		listaOrdenada = burbuja.sort(aordenar);
		
		assertArrayEquals(ListaEsperada, listaOrdenada);
		
	}

}
